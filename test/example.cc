
#include <iostream>
#include <fstream>
#include <chrono>

#include "muson/muson.h"
#include "fmt.h"

#include "rapidjson/document.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/writer.h"

/* 
 * libc number parsing and formating routines are brokenly slow, here are good replacements.
 *
 * (Only needed because this is a benchmark against the world's fastest JSON parser.)
 */

struct FastStringStream {
    std::string data;

    FastStringStream& operator<<(char c) {
        data += c;
        return *this;
    }

    FastStringStream& operator<<(const std::string& s) {
        data += s;
        return *this;
    }

    template <typename T>
    FastStringStream& operator<<(T v) {
        fmt::format_numeric(data, v);
        return *this;
    }

    FastStringStream& operator<<(double v) {
        fmt::format_real(data, v, "");
        return *this;
    }

    const std::string& str() const { return data; }
};

template <typename I>
struct FastReader : public muson::Reader<I> {
    using muson::Reader<I>::Reader;

    int64_t to_int(const I& b, const I& e) {
        return fmt::parse<int64_t>(b);
    }
};


/* */


void test_muson(const std::string& data, size_t N) {

    using OpenRTBSegment = muson::Signature<LIT("{data:[{id:String,segment:[{ext:{lm:Int,prob:?Real},id:Int}]}]}")>;

    static const std::string ascii_sig = OpenRTBSegment::print_signature();

    struct accessor {
        struct data_t {
            std::string id;
            struct segment_t {
                struct {
                    int64_t lm;
                    struct {
                        bool null;
                        double val;
                    } prob;
                } ext;
                int64_t id;
            };
            std::vector<segment_t> segment;
        };
        std::vector<data_t> data;
    };
    

    for (size_t z = 0; z < N; ++z) {

        size_t n = 0;

        OpenRTBSegment parsed;

        FastReader<std::string::const_iterator> reader(data.begin(), data.end());
        muson::decode(ascii_sig, reader, parsed);

        // Use the simpler form if you don't care about performance:
        //   muson::decode(ascii_sig, data, parsed);

        accessor& x = muson::cast<accessor>(parsed);

        for (auto& a : x.data) {
            for (auto& b : a.segment) {
                if (!b.ext.prob.null) {
                    ++n;

                    if (z > 0) {
                        b.ext.prob.null = true;
                    }
                }
            }
        }

        /*
         * Alternatively, if you want to access data the hard way:
         *
            const auto& a = MUGET(parsed, "data"); 
            for (const auto& b : a.data) {

                const auto& c = MUGET(b, "segment");
                for (const auto& d : c.data) {

                    if (!MUGET(MUGET(d, "ext"), "prob").null) {
                        ++n;
                    }
                }
            }
         *
         *   MUGET(x, "f") is a macro that's equivalent to muson::get<LIT("f")>(x).
         *
         */

        FastStringStream ss;
        muson::encode(ascii_sig, ss, parsed);

        if (z == 0) {
            if (n != 103)
                throw std::runtime_error("Failed to count properly.");

            std::cout << "μ-son encoded size in bytes: " << ss.str().size() << std::endl;

            if (ss.str() + "\n" != data) {
                std::cout << ss.str() << std::endl;
                throw std::runtime_error("Sanity checking for encoding failed.");
            }

            if (ascii_sig != "{data:[{id:String,segment:[{ext:{lm:Int,prob:?Real},id:Int}]}]}") {
                throw std::runtime_error("Sanity checking for signature failed.");
            }
        }
    }
}

void test_rapidjson(const std::string& data, size_t N) {

    for (size_t z = 0; z < N; ++z) {

        size_t n = 0;

        rapidjson::Document d;
        if (d.Parse(data.c_str()).HasParseError())
            throw std::runtime_error("Bad JSON.");

        auto& a = d["data"];
        for (rapidjson::SizeType i = 0; i < a.Size(); ++i) {

            auto& b = a[i]["segment"];
            for (rapidjson::SizeType j = 0; j < b.Size(); ++j) {

                auto& c = b[j]["ext"];
                if (c.HasMember("prob")) {
                    ++n;

                    if (z > 0) {
                        c["prob"].SetNull();
                    }
                }
            }
        }

        rapidjson::StringBuffer sb;
        rapidjson::Writer<rapidjson::StringBuffer> writer(sb);
        d.Accept(writer);
        
        if (z == 0) {
            if (n != 103)
                throw std::runtime_error("Failed to count properly.");

            std::cout << "JSON encoded size in bytes: " << ::strlen(sb.GetString()) << std::endl;
        }
    }
}

int main(int argc, char** argv) {

    try {

        constexpr size_t N = 50000;

        if (argc != 3) {
            throw std::runtime_error("Usage:\n"
                                     "    ./example muson data/openrtb_user_data.muson\n"
                                     "or\n"
                                     "    ./example rapidjson data/openrtb_user_data.json");
        }

        std::ifstream f(argv[2]);
        std::string data(std::istreambuf_iterator<char>(f), (std::istreambuf_iterator<char>()));

        std::string what{argv[1]};

        std::cout << "Microbenchmarking..." << std::endl;

        auto start_time = std::chrono::system_clock::now();

        if (what == "muson") {
            test_muson(data, N);

        } else if (what == "rapidjson") {
            test_rapidjson(data, N);
        }

        auto timediff = std::chrono::system_clock::now() - start_time;

        std::cout << "Time elapsed for " << N << " iterations: "
                  << std::chrono::duration_cast<std::chrono::milliseconds>(timediff).count()
                  << " milliseconds." << std::endl;

    } catch (std::exception& e) {
        std::cerr << "ERROR: " << e.what() << std::endl;
        return 1;
    }

    return 0;
}

